package Assignment;

import org.testng.annotations.Test;

import static io.restassured.RestAssured.baseURI;
import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.greaterThanOrEqualTo;

import org.json.simple.JSONObject;

public class PutOperation {
	 @Test
	  public void testPutOperation() {
		  
		  JSONObject req = new JSONObject();
		  req.put("name", "Parithi");
		  req.put("email", "elam33@gmail.com");
		  req.put("gender", "male");
		  req.put("status", "Active");
			
			baseURI = "https://gorest.co.in/public/v2";
			given().log().all().contentType("application/json")
			.header("authorization", "Bearer c829f35b8876819ec8b1d45be478359e43b04b8cce5afb87c6a017302ecf16b5")
			.body(req.toJSONString()).patch("/users/199397").then().statusCode(200);
	 	  
	  }
  
}
