package Assignment;

import org.testng.annotations.Test;

import static io.restassured.RestAssured.baseURI;
import static io.restassured.RestAssured.given;

public class DeleteOPeration {
	@Test
	public void f() {
		
		baseURI = "https://gorest.co.in/public/v2";
		given().log().all().contentType("application/json")
				.header("authorization", "Bearer c829f35b8876819ec8b1d45be478359e43b04b8cce5afb87c6a017302ecf16b5")
				.delete("/users/199397").then().statusCode(204);
	}
 
}
