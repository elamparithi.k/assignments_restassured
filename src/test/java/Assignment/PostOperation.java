package Assignment;

import org.json.simple.JSONObject;
import org.testng.annotations.Test;
import static io.restassured.RestAssured.*;
import io.restassured.response.ValidatableResponse;

public class PostOperation {
	@Test
	public void testingPostOperation() {
		JSONObject req = new JSONObject();
		req.put("name", "Elam");
		req.put("email", "elam33@gmail.com");
		req.put("gender", "male");
		req.put("status", "Active");
		System.out.println(req);
		baseURI = "https://gorest.co.in/public/v2";
		ValidatableResponse Response = given().log().all().contentType("application/json")
				.header("authorization", "Bearer c829f35b8876819ec8b1d45be478359e43b04b8cce5afb87c6a017302ecf16b5")
				.body(req.toJSONString()).when().post("/users").then().statusCode(201);
		
		

	}
 
}
